﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkerData : MonoBehaviour
{
    public Transform osbtacleEndMarker;
    public Transform playerEndMarker;
}

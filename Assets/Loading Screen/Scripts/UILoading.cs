﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILoading : MonoBehaviour
{
    [SerializeField] SceneLoader sceneLoader;
	// Use this for initialization
	void Start ()
    {
        sceneLoader.LoadMainScene();
	}
}

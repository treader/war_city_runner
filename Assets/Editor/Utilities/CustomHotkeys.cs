﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

public class CustomHotkeys : MonoBehaviour
{
    [MenuItem("Tools/Scene/StartUp %#t")]
    static void OpenStartUpScene()
    {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/Scenes/StartUp.unity");
    }

    [MenuItem("Tools/Scene/Main %#m")]
    static void OpenMainScene()
    {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/Scenes/Main.unity");
    }
     
    [MenuItem("Tools/Scene/Play %#r")]
    static void Play()
    {
        OpenStartUpScene();
        EditorApplication.isPlaying = true;
    }
}

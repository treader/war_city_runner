﻿using System.Collections;
using UnityEditor;
using UnityEngine;

public class PlayerConfigBuilder
{
    [MenuItem("Assets/Create/Config/PlayerConfig")]
    public static void CreateMyAsset()
    {
        PlayerConfig asset = ScriptableObject.CreateInstance<PlayerConfig>();

        AssetDatabase.CreateAsset(asset, "Assets/Gameplay/Character/Config/PlayerConfig.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}

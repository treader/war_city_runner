﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class WorldConfigBuilder : MonoBehaviour
{
    [MenuItem("Assets/Create/Config/WorldConfig")]
    public static void CreateMyAsset()
    {
        WorldConfig asset = ScriptableObject.CreateInstance<WorldConfig>();

        AssetDatabase.CreateAsset(asset, "Assets/Gameplay/Config/WorldConfig.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}

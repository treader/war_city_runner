﻿using UnityEditor;
using UnityEngine;

public class ObstacleConfigBuilder : MonoBehaviour
{
    [MenuItem("Assets/Create/Config/ObstacleConfig")]
    public static void CreateMyAsset()
    {
        ObstacleConfig asset = ScriptableObject.CreateInstance<ObstacleConfig>();

        AssetDatabase.CreateAsset(asset, "Assets/Gameplay/Obstacle/Config/ObstacleConfig.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}

﻿using System.Collections.Generic;
using UnityEngine;

public class BoosterPool : MonoBehaviour
{
    [SerializeField] Booster boosterTemplate;
    [SerializeField] List<Booster> boosterPool;

    Vector3 boosterPosition;

    // Start is called before the first frame update
    void Start()
    {
        boosterPool = new List<Booster>();
        boosterPosition = Vector3.zero;
    }

    public void TriggerBoost(Vector3 position)
    {
        Booster booster = null;
        boosterPosition = position;

        booster = FetchFromPool();
        if (booster == null)
        {
            InstantiateBooster();
        }
        else
        {
            InitBooster(booster);
        }
    }

    void InstantiateBooster()
    {
        Booster booster = GameObject.Instantiate<Booster>(boosterTemplate, transform, true);
        InitBooster(booster);
        boosterPool.Add(booster);
    }

    Booster FetchFromPool()
    {
        Booster booster = null;
        for (int i = 0; i < boosterPool.Count; i++)
        {
            booster = boosterPool[i];

            if (!booster.IsEnabled())
            {
                return booster;
            }
        }

        return null;
    }

    void InitBooster(Booster booster)
    {
        booster.Play();
        booster.transform.position = boosterPosition;
    }
}

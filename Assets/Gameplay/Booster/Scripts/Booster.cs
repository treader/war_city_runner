﻿using UnityEngine;

public class Booster : MonoBehaviour
{
    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] Animator animator;

    public void Play()
    {
        animator.SetTrigger("enable");
    }

    public bool IsEnabled()
    {
        return spriteRenderer.enabled;
    }
}

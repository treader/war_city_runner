﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO: Need a better way to handle ScoreKeeper refrence
public class ScoreKeeper : MonoBehaviour, IScoreKeeperData
{
    int score = 0;
    float currentTime = 0;
    bool canUpdateScore = false;

    public int pScore
    {
        get
        {
            return score;
        }
    }

    public void BeginScoreKeeping()
    {
        ResetScore();
        canUpdateScore = true;
    }

    public void StopScoreKeeping()
    {
        canUpdateScore = false;
        currentTime = 0;
    }

    void ResetScore()
    {
        score = 0;
        currentTime = 0;
        canUpdateScore = false;
    }

    void Update()
    {
        if (canUpdateScore)
        {
            UpdateScore();
        }
    }

    void UpdateScore()
    {
        currentTime += Time.deltaTime;

        if(currentTime >= 1)
        {
            score++;
            currentTime = 0;
        }
    }
}

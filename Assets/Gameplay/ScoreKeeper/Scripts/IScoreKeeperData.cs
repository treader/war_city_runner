﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IScoreKeeperData
{
    int pScore {  get;  }
}

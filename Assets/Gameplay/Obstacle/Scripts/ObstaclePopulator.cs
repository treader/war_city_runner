﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclePopulator : MonoBehaviour
{
    [SerializeField] Obstacle obstacleTemplate;
    [SerializeField] float timeInterval; //TODO: This should come from config

    List<Obstacle> obstaclePool;
	// Use this for initialization
	void Start ()
    {
        obstaclePool = new List<Obstacle>();
        StartCoroutine(PopulateAtInterval());
	}

    void InitializeObstacle()
    {
        Obstacle obstacle;
        if (obstaclePool.Count == 0)
        {
            obstacle = GameObject.Instantiate<Obstacle>(obstacleTemplate, transform);
        }
        else
        {
            obstacle = obstaclePool[0];
        }

        obstacle.gameObject.SetActive(true);

        obstacle.OnReachedEnd = HandleObstacleReachedEnd;

        if (obstaclePool.Contains(obstacle))
        {
            obstaclePool.Remove(obstacle);
        }
    }

    void HandleObstacleReachedEnd(Obstacle obstacle)
    {
        obstaclePool.Add(obstacle);
        obstacle.gameObject.SetActive(false);
        obstacle.OnReachedEnd = null;
    }

    IEnumerator PopulateAtInterval()
    {
        yield return new WaitForSeconds(1);
        while (true)
        {
            yield return new WaitForSeconds(timeInterval);

            InitializeObstacle();
        }
    }
}

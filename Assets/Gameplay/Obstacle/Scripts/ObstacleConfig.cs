﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleConfig : ScriptableObject
{
    public float horizontalSpeed = 6;
    public Vector3 initialPosition;
}

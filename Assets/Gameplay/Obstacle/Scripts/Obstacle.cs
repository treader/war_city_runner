﻿using System;
using UnityEngine;
using Treader;

public class Obstacle : MonoBehaviour
{
    public Action<Obstacle> OnReachedEnd;

    [SerializeField] GameObject endPoint;

    Vector3 moveVector;

    WorldConfig worldConfig;
    ObstacleConfig config;

    private void Start()
    {
        worldConfig = (WorldConfig)ServiceLocator.Instance.GetService<ConfigCatalog>(false).GetConfig<WorldConfig>();
        config = (ObstacleConfig)ServiceLocator.Instance.GetService<ConfigCatalog>(false).GetConfig<ObstacleConfig>();
        moveVector = config.initialPosition;
    }

    void Move()
    {
        moveVector.x = moveVector.x - (worldConfig.worldBaseSpeed * Time.fixedDeltaTime);
        transform.position = moveVector;
    }

    private void FixedUpdate()
    {
        Move();
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.gameObject == endPoint)
        {
            moveVector = config.initialPosition;
            if (OnReachedEnd != null)
            {
                OnReachedEnd(this);
            }
        }
    }
}

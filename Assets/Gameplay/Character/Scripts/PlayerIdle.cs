﻿using System;
using System.Collections;
using Treader;
using UnityEngine;

public class PlayerIdle : MonoBehaviour
{
    [SerializeField] PlayerData playerData;

    WorldConfig worldConfig;

    Vector3 idleMoveVector;
    bool isObstacleColliding = false;
    bool isAtEnd = false;

    // Use this for initialization
    void Start ()
    {
        worldConfig = (WorldConfig)ServiceLocator.Instance.GetService<ConfigCatalog>(false).GetConfig<WorldConfig>();
        playerData.playerCollisionEvents.OnCollideObstacleLeft += HandleCollisionObstacleColliding;
        playerData.playerCollisionEvents.OnCollideObstacleLeft += HandleCollisionObstacleColliding;
        playerData.playerCollisionEvents.OnObstacleCollisionExit += HandleObstacleCollisionExit;
        playerData.playerCollisionEvents.OnPlayerAtEnd += HandlePlayerAtEnd;
        idleMoveVector = transform.position;	
	}

    // Update is called once per frame
    void FixedUpdate ()
    {
        if (IsIdle())
        {
            DoIdle();
        }
    }

    void HandleCollisionObstacleColliding()
    {
        isObstacleColliding = true;
    }

    void HandleObstacleCollisionExit()
    {
        isObstacleColliding = false;
    }

    void HandlePlayerAtEnd (bool atEnd)
    {
        isAtEnd = atEnd;
    }

    bool IsIdle()
    {
        Transform endMarker = ServiceLocator.Instance.GetService<MarkerData>(false).playerEndMarker;
        //isAtEnd = endMarker != null && VectorMath.GetDistanceAlongX(transform.position, endMarker.position) < 0.5f;

        if ((Input.GetButton("Horizontal") && !isObstacleColliding) || isAtEnd)
        {
            return false;
        }
        return true;
    }

    void DoIdle()
    {
        idleMoveVector = transform.position;
        idleMoveVector.x = idleMoveVector.x - (worldConfig.worldBaseSpeed * Time.fixedDeltaTime);
        transform.position = idleMoveVector;
    }
}

﻿using System.Collections;
using System;
using UnityEngine;
using Treader;

public class PlayerCollisionEvents : MonoBehaviour
{
    public Action<GameObject> OnTopOfSomething;
    public Action OnCollideObstacleLeft;
    public Action OnCollideObstacleRight;
    public Action OnObstacleCollisionExit;

    public Action<bool> OnPlayerAtEnd;

    [SerializeField] LayerMask mixedSafeMask;
    [SerializeField] LayerMask safeMask;
    [SerializeField] LayerMask endMask;

    Collider2D eventsCollider;
    Collider2D eventsColliderLeft = null;

    bool isAtEnd = false;

    void OnTriggerEnter2D(Collider2D collision)
    {
        eventsCollider = collision;

        if (IsOnTop(mixedSafeMask))
        {
            if (OnTopOfSomething != null)
            {
                OnTopOfSomething(collision.gameObject);
            }
        }

        if (IsCollidingLayer(endMask))
        {
            DoAtEndEvent(true);
        }
        
        if (IsObstacleLeft(eventsCollider) || isAtEnd)
        {
            if (IsObstacleLeft(eventsCollider) && OnCollideObstacleLeft != null)
            {
                eventsColliderLeft = eventsCollider;
                OnCollideObstacleLeft();
            }

            if (isAtEnd && IsObstacleLeft(eventsColliderLeft) && !IsOnTop(mixedSafeMask)) //This will happen when the player dies
            {
                isAtEnd = false;
                TreaderFSM.Instance.ChangeState(States.END_SCREEN_STATE);
            }
        }

        if (IsObstacleRight() && !IsOnTop(safeMask))
        {
            if (OnCollideObstacleRight != null)
            {
                OnCollideObstacleRight();
            }
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        DoAtEndEvent(false);
        eventsColliderLeft = null;
        if (((1 << eventsCollider.gameObject.layer) & safeMask) != 0)
        {
            if (OnObstacleCollisionExit != null)
            {
                OnObstacleCollisionExit();
            }
        }
    }

    bool IsOnTop(LayerMask layerMask)
    {
        if (((1 << eventsCollider.gameObject.layer) & layerMask) != 0)
        {
            if (VectorMath.GetDistanceAlongY(transform.position, eventsCollider.transform.position) > eventsCollider.bounds.extents.y)
            {
                return true;
            }
        }
        return false;
    }

    bool IsObstacleLeft(Collider2D eventsCollider)
    {
        if (eventsCollider != null && ((1 << eventsCollider.gameObject.layer) & safeMask) != 0)
        {
            if (VectorMath.GetDistanceAlongX(transform.position, eventsCollider.transform.position) > eventsCollider.bounds.extents.x)
            {
                return true;
            }
        }
        return false;
    }

    bool IsObstacleRight()
    {
        if (((1 << eventsCollider.gameObject.layer) & safeMask) != 0)
        {
            if (transform.position.x < eventsCollider.transform.position.x)
            {
                return true;
            }
        }
        return false;
    }

    bool IsCollidingLayer(LayerMask layerMask)
    {
        if (((1 << eventsCollider.gameObject.layer) & layerMask) != 0)
        {
            return true;
        }

        return false;
    }

    void DoAtEndEvent(bool atEnd)
    {
        //Transform endMarker = ServiceLocator.Instance.GetService<MarkerData>(false).playerEndMarker;
        //isAtEnd = endMarker != null && VectorMath.GetDistanceAlongX(transform.position, endMarker.position) < 0.5f;
        isAtEnd = atEnd;

        if (OnPlayerAtEnd != null)
        {
            OnPlayerAtEnd(isAtEnd);
        }
    }
}

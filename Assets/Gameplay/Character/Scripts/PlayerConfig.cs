﻿using UnityEngine;

public class PlayerConfig : ScriptableObject
{
    public float jumpForce = 20;
    public float speed = 20;
    public float gravityModifier = 0.5f;
}

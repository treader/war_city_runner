﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimatorKeys
{
    public const string SPEED = "Speed";
    public const string GROUNDED = "isOnGround";
    public const string JUMPSPEED = "jSpeed";
}

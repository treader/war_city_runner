﻿using Treader;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    public Animator mainAnimator;
    public Animator cloudAnimator;

    private PlayerConfig playerConfig;
    public PlayerCollisionEvents playerCollisionEvents;

    public PlayerConfig pPlayerConfig
    {
        get
        {
            if (playerConfig != null)
            {
                return playerConfig;
            }
            else
            {
                playerConfig = (PlayerConfig)ServiceLocator.Instance.GetService<ConfigCatalog>(false).GetConfig<PlayerConfig>();
                return playerConfig;
            }
        }
    }
}

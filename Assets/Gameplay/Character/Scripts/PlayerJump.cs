﻿using UnityEngine;
using Treader;

public class PlayerJump : MonoBehaviour
{
    [SerializeField] PlayerData playerData;

    bool canJump = true;
    bool doubleJump = true;
    bool onTopOfObstacle = false;

    Vector3 jumpVector;
    Vector3 fallVector;

    float currentForce;

    BoosterPool boosterPool;

    private void Start()
    {
        playerData.playerCollisionEvents.OnTopOfSomething += HandleOnTopOfSomething;
        playerData.playerCollisionEvents.OnObstacleCollisionExit += HandleObstacleCollisionExit;

        boosterPool = ServiceLocator.Instance.GetService<BoosterPool>(false);
    }

    void InitJump()
    {
        jumpVector = transform.position;
        currentForce = playerData.pPlayerConfig.jumpForce;
 
        if (!doubleJump)
        {
            canJump = false;

            boosterPool.TriggerBoost(transform.position);
        }
        else
        {
            doubleJump = false;
        }
    }

    void InitFall()
    {
        jumpVector = transform.position;
        currentForce = 0;
        doubleJump = false;
    }

    void DoJump()
    {
        playerData.mainAnimator.SetBool(PlayerAnimatorKeys.GROUNDED, false);
        playerData.mainAnimator.SetFloat(PlayerAnimatorKeys.JUMPSPEED, currentForce);

        currentForce -= playerData.pPlayerConfig.gravityModifier;
        jumpVector.y = (transform.position.y + (currentForce * Time.fixedDeltaTime));
        jumpVector.x = transform.position.x;
        transform.position = jumpVector;
    }

    void FixedUpdate()
    {
        if (Input.GetButtonDown("Action") && canJump)
        {
            InitJump();
        }

        if (!canJump || !doubleJump)
        {
            DoJump();
        }
    }

    void HandleOnTopOfSomething(GameObject topOfObject)
    {
        canJump = true;
        doubleJump = true;
        playerData.mainAnimator.SetBool(PlayerAnimatorKeys.GROUNDED, true);
        onTopOfObstacle = topOfObject.GetComponent<Obstacle>() != null;
    }

    void HandleObstacleCollisionExit()
    {
        if (onTopOfObstacle && canJump && doubleJump)
        {
            InitFall();
        }
    }
}

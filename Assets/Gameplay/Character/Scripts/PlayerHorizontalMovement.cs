﻿using System;
using UnityEngine;

public class PlayerHorizontalMovement : MonoBehaviour
{
    [SerializeField] PlayerData playerData;

    Vector3 moveVector;
    bool clampRight = false;
    bool clampLeft = false;

    // Start is called before the first frame update
    void Start()
    {
        Init();
        playerData.playerCollisionEvents.OnCollideObstacleRight += HandlePlayerCollisionRight;
        playerData.playerCollisionEvents.OnCollideObstacleLeft += HandlePlayerCollisionLeft;
        playerData.playerCollisionEvents.OnObstacleCollisionExit += HandleCollisionExit;
    }

    public void Init()
    {
        moveVector = Vector3.zero;
        clampRight = false;
        clampLeft = false;
    }

    void HandleCollisionExit()
    {
        clampRight = false;
        clampLeft = false;
    }

    void HandlePlayerCollisionRight()
    {
        clampRight = true;
    }
    
    void HandlePlayerCollisionLeft()
    {
        clampLeft = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Horizontal") != 0)
        {
            DoMovement();
        }
    }

    void DoMovement()
    {
        float horizontalAxis = Input.GetAxis("Horizontal");

        if (clampRight)
        {
            horizontalAxis = Mathf.Clamp(horizontalAxis, -1f, 0f);
        }
        else if (clampLeft)
        {
            horizontalAxis = Mathf.Clamp(horizontalAxis, 0f, 1f);
        }

        moveVector.x = (horizontalAxis * playerData.pPlayerConfig.speed * Time.fixedDeltaTime) + transform.localPosition.x;
        moveVector.y = transform.localPosition.y;

        transform.localPosition = moveVector;
    }
}

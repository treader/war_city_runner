﻿using Treader;
using UnityEngine;

public class GameplayLogic : StateBase
{
    [SerializeField] GameObject playerCharacter;

    public override void Init()
    {
        base.Init();
        playerCharacter.SetActive(true);
        ServiceLocator.Instance.GetService<ScoreKeeper>(true).BeginScoreKeeping();
        ServiceLocator.Instance.GetService<PlayerHorizontalMovement>(false).Init();
    }

    public override void DeInit()
    {
        base.DeInit();
        playerCharacter.SetActive(false);
        ServiceLocator.Instance.GetService<ScoreKeeper>(true).StopScoreKeeping();
    }
}

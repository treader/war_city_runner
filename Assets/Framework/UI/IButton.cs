﻿
namespace Treader.UI
{
    public interface IButton
    {
        void OnButtonSelect();
    }
}

﻿using System.Collections.Generic;
using UnityEngine;

namespace Treader
{
    public class TreaderFSM : MonoBehaviour
    {
        [SerializeField] List<StateBase> stateBases;
        [SerializeField] string defaultState;
        private StateBase currentState;

        public static TreaderFSM Instance { get; private set; }

        public void Awake()
        {
            Instance = this;
            //if (!string.IsNullOrEmpty(defaultState))
            //{
            //    ChangeState(defaultState);
            //}
        }

        public void ChangeState(string stateName)
        {
            for (int i = 0; i < stateBases.Count; i++)
            {
                if (stateBases[i].stateName.Equals(stateName))
                {
                    if (currentState != null)
                    {
                        currentState.DeInit();
                    }

                    currentState = stateBases[i];
                    currentState.Init();
                }
            }
        }
    }
}

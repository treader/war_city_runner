﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class States
{
    public const string MAIN_MENU_STATE = "MainMenu";
    public const string GAMEPLAY_STATE = "Gameplay";
    public const string END_SCREEN_STATE = "EndScreen";
}

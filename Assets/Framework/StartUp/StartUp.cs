﻿using System;
using System.Collections;
using Treader;
using UnityEngine;

public class StartUp : MonoBehaviour
{
    [SerializeField] SceneLoader sceneLoader;
	// Use this for initialization
	void Start ()
    {
        sceneLoader.OnSceneLoadDone += HandleSceneLoaded;	
	}

    private void HandleSceneLoaded()
    {
        StartCoroutine(WaitAndSetState());
        sceneLoader.OnSceneLoadDone -= HandleSceneLoaded;
    }

    IEnumerator WaitAndSetState()
    {
        yield return new WaitForSeconds(0.1f);
        TreaderFSM.Instance.ChangeState(States.MAIN_MENU_STATE);
    }
}

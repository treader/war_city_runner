﻿#define DEBUG_LOAD

using System.Collections;
using UnityEngine;
using System;
using System.IO;

public class AsyncBundleLoader : MonoBehaviour
{
    Action<bool, UnityEngine.Object> OnResourceLoaded;
    public AsyncBundleLoader instance;
    string path;

#if DEBUG_LOAD
    float timeToLoadBundle = 0;
#endif

    private void Start()
    {
        instance = this;
    }

    //Only loads the assetbundle not an asset inside the bundle
    public void LoadBundle(string path, Action<bool, UnityEngine.Object> callback)
    {
        OnResourceLoaded = callback;
        this.path = path;
#if DEBUG_LOAD
        timeToLoadBundle = 0;
#endif
        StartCoroutine(StartAsyncLoad());
    }

    IEnumerator StartAsyncLoad()
    {
        var bundleLoadRequest = AssetBundle.LoadFromFileAsync(Path.Combine(Application.streamingAssetsPath, path));

#if DEBUG_LOAD
        timeToLoadBundle += Time.deltaTime;
#endif
        yield return bundleLoadRequest;

        AssetBundle myLoadedAssetBundle = bundleLoadRequest.assetBundle;
        if (myLoadedAssetBundle == null)
        {
            Debug.LogError("Failed to load AssetBundle at path: "+path);
            OnResourceLoaded(false, null);
            OnResourceLoaded = null;
            yield break;
        }
        
        OnResourceLoaded(true, myLoadedAssetBundle);
        OnResourceLoaded = null;
#if DEBUG_LOAD
        Debug.Log("Time to load scene bundle is: " + timeToLoadBundle);
#endif
        //myLoadedAssetBundle.Unload(false);
    }
}
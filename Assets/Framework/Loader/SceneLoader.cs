﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public Action OnSceneLoadDone;
    [SerializeField] AsyncBundleLoader asyncBundleLoader;

    const string path = "mainscene";
    string currentScenePath = string.Empty;

    AssetBundle mainSceneBundle = null;
    AsyncOperation asyncLoad;
    
    float timeToLoadScene = 0;

    public void LoadMainScene()
    {
        timeToLoadScene = 0;
        asyncBundleLoader.LoadBundle(path, HandleMainSceneLoaded);
    }

    private void HandleMainSceneLoaded(bool isLoaded, UnityEngine.Object inObj)
    {
        if (isLoaded)
        {
            mainSceneBundle = inObj as AssetBundle;
            StartCoroutine(LoadYourAsyncScene());
        }
        else
        {
            Debug.LogError("Failed to load the main scene");
        }
    }

    IEnumerator LoadYourAsyncScene()
    {
        currentScenePath = mainSceneBundle.GetAllScenePaths()[0];

        asyncLoad = SceneManager.LoadSceneAsync(currentScenePath);
        asyncLoad.completed += HandleSceneLoaded;
        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            timeToLoadScene += Time.deltaTime;
            yield return null;
        }
    }

    void HandleSceneLoaded(AsyncOperation asyncOperation)
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByPath(currentScenePath));
        Debug.Log("Time to load scene: " + timeToLoadScene);

        asyncLoad.completed -= HandleSceneLoaded;
        if (OnSceneLoadDone != null)
        {
            OnSceneLoadDone();
        }
    }
}

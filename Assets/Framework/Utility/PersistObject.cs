﻿using UnityEngine;

//Simple helper component class to keep the game object in scene
public class PersistObject : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }
}

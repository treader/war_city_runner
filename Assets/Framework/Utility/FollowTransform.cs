﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTransform : MonoBehaviour
{
    [SerializeField] Transform transformToFollow;
    [SerializeField] bool x,y,z;

    Vector3 followPosition;
	
	// Update is called once per frame
	void Update ()
    {
        DoFollow();
    }

    void DoFollow()
    {
        followPosition.x = x ? transformToFollow.position.x : transform.position.x;
        followPosition.y = y ? transformToFollow.position.y : transform.position.y;
        followPosition.z = z ? transformToFollow.position.z : transform.position.z;

        transform.position = followPosition;
    }
}

﻿using UnityEngine;
using System;

namespace Treader.Utils
{
	public class FormatText 
	{
		public static string FormatTimeMilliSeconds(double milliSeconds)
		{
			string formatedText = "";
            int seconds = (int)milliSeconds;
            int fraction = (int)(milliSeconds * 1000);

			formatedText = string.Format("{0:D2}:{1:D2}:{2:00}", (int)seconds / 60, seconds % 60, (fraction % 100));

			return formatedText;
		}
	}
}

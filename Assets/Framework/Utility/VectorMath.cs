﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VectorMath
{
    public static float GetDistanceAlongY(Vector3 a, Vector3 b)
    {
        Vector3 difference;
        difference = a - b;

        return Mathf.Abs(difference.y);
    }

    public static float GetDistanceAlongX(Vector3 a, Vector3 b)
    {
        Vector3 difference;
        difference = a - b;

        return Mathf.Abs(difference.x);
    }
}

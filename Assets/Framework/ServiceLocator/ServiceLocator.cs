﻿using System.Collections.Generic;
using UnityEngine;
using System;

namespace Treader
{
    public class ServiceLocator : MonoBehaviour
    {
        public static ServiceLocator Instance;
        Dictionary<object, MonoBehaviour> services;
        bool canCreateIfNotFound = false;

        public T GetService<T>(bool createIfNotFound) where T : MonoBehaviour
        {
            canCreateIfNotFound = createIfNotFound;

            if (services == null)
            {
                services = new Dictionary<object, MonoBehaviour>();
            }
            try
            {
                if (services.ContainsKey(typeof(T)))
                {
                    T service = (T)services[typeof(T)];
                    if (service != null)
                    {
                        return service;
                    }
                    else
                    {
                        services.Remove(typeof(T));
                        return FindService<T>();
                    }
                }
                else
                {
                    return FindService<T>();
                }
            }
            catch
            {
                throw new Exception();
            }
        }

        T FindService<T>() where T : MonoBehaviour
        {
            T service = GameObject.FindObjectOfType<T>();
            if (service != null)
            {
                services.Add(typeof(T), service);
            }
            else if (canCreateIfNotFound)
            {
                service = gameObject.AddComponent<T>();
                services.Add(typeof(T), service);
            }

            return (T)services[typeof(T)];
        }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }
    }
}

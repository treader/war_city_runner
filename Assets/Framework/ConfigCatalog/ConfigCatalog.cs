﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ConfigCatalog : MonoBehaviour
{
    [SerializeField] ScriptableObject[] configs;

    public T GetConfig<T>() where T : ScriptableObject
    {
        try
        {
            for (int i = 0; i < configs.Length; i++)
            {
                ScriptableObject config = configs[i];
                if (config != null && config is T)
                {
                    return (T)config;
                }
            }

            return null;
        }
        catch
        {
            throw new Exception();
        }
    }
}

﻿using UnityEngine;
using Treader.UI;
using Treader;

public class StartButton : MonoBehaviour, IButton
{
    public void OnButtonSelect()
    {
        TreaderFSM.Instance.ChangeState(States.GAMEPLAY_STATE);
    }
}

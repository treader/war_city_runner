﻿using System.Collections.Generic;
using Treader.UI;
using UnityEngine;
using Treader;

public class UiMainMenu : MonoBehaviour
{
    [SerializeField] GameObject[] buttonsGameObjects;
    [SerializeField] Transform selector;

    List<IButton> buttons;
    int selectedIndex = 0;

    // Use this for initialization
    void Start ()
    {
        buttons = new List<IButton>();
        RegisterButtons();
        UpdateSelector(0);
	}

    void RegisterButtons()
    {
        buttons.Clear();
        if (buttonsGameObjects != null)
        {
            for (int i = 0; i < buttonsGameObjects.Length; i++)
            {
                IButton button;
                button = buttonsGameObjects[i].GetComponent<IButton>();

                Debug.Assert(button != null, "Failed to add buttons please makes sure all buttons inherit from IButton");

                if (button != null)
                {
                    buttons.Add(button);
                }
            }
        }
    }

    void UpdateSelector(int index)
    {
        if (index < buttonsGameObjects.Length)
        {
            selector.position = buttonsGameObjects[index].transform.position;
            selectedIndex = index;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            buttons[selectedIndex].OnButtonSelect();
        }
    }
}

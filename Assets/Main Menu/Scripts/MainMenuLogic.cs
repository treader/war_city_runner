﻿using Treader;
using UnityEngine;

public class MainMenuLogic : StateBase
{
    [SerializeField] UiMainMenu mainMenu;
    public override void Init()
    {
        base.Init();
        if (mainMenu != null)
        {
            mainMenu.gameObject.SetActive(true);
        }
        else
        {
            Debug.LogError("Main menu referance missing: "+gameObject.GetInstanceID());
        }
    }

    public override void DeInit()
    {
        base.DeInit();
        mainMenu.gameObject.SetActive(false);
    }
}

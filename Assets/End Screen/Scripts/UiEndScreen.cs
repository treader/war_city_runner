﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Treader;

public class UiEndScreen : MonoBehaviour
{
    [SerializeField] Text scoreText;
    [SerializeField] Animator animator;

	// Use this for initialization
	public void Init ()
    {
        IScoreKeeperData scoreKeeper = (IScoreKeeperData)ServiceLocator.Instance.GetService<ScoreKeeper>(true);
        SetScoreText(scoreKeeper);

        gameObject.SetActive(true);
        animator.SetTrigger("In");
    }

    public void DeInit()
    {
        gameObject.SetActive(false);
    }

    void SetScoreText(IScoreKeeperData scoreKeeper)
    {
        scoreText.text = string.Format("You lasted {0} seconds", scoreKeeper.pScore);
    }

    private void Update()
    {
        if (Input.GetButtonDown("Action"))
        {
            TreaderFSM.Instance.ChangeState(States.GAMEPLAY_STATE);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TreaderFSM.Instance.ChangeState(States.MAIN_MENU_STATE);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Treader;

public class EndScreenLogic : StateBase
{
    [SerializeField] UiEndScreen endScreen;

    public override void Init()
    {
        base.Init();
        endScreen.Init();
    }

    public override void DeInit()
    {
        base.DeInit();
        endScreen.DeInit();
    }
}
